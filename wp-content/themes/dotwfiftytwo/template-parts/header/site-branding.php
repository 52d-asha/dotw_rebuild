<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage dotwfiftytwo
 * 
 * @since 1.0.0
 */
?>
<div class="site-branding">

	<?php if ( has_custom_logo() ) : ?>
		<div class="site-logo"><?php the_custom_logo(); ?></div>
	<?php endif; ?>

	



		<nav id="site-navigation" class="main-navigation" aria-label="">
			<div class="top-menu">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_class'     => 'main-menu',
					'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				)
			);
			get_template_part( 'template-parts/header/header', 'widgets' );
			?>
			</div>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'secondary',
					'menu_class'     => 'secondary-menu',
					'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'item_spacing'   => "discard",
				)
			);
			?>
		</nav><!-- #site-navigation -->

	<?php if ( has_nav_menu( 'social' ) ) : ?>
		<nav class="social-navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'dotwfiftytwo' ); ?>">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'social',
					'menu_class'     => 'social-links-menu',
					'link_before'    => '<span class="screen-reader-text">',
					'link_after'     => '</span>' . twentynineteen_get_icon_svg( 'link' ),
					'depth'          => 1,
				)
			);
			?>
		</nav><!-- .social-navigation -->
	<?php endif; ?>
</div><!-- .site-branding -->
