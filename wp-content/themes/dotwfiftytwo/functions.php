<?php
//* Code goes here

/**
 * dotwfiftytwo's functions and definitions
 *
 * @package WordPress
 * @since dotwfiftytwo 1.0
 */
 
/**
 * First, let's set the maximum content width based on the theme's design and stylesheet.
 * This will limit the width of all uploaded images and embeds.
 */
if ( ! isset( $content_width ) )
    $content_width = 800; /* pixels */
 
if ( ! function_exists( 'dotwfiftytwo_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function dotwfiftytwo_setup() {
 
    /**
     * Make theme available for translation.
     * Translations can be placed in the /languages/ directory.
     */
    load_theme_textdomain( 'dotwfiftytwo', get_template_directory() . '/languages' );
 
    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );
 
    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );
 
    /**
     * Add support for two custom navigation menus.
     */
    // register_nav_menus( array(
    //     'primary'   => __( 'Primary Menu', 'dotwfiftytwo' ),
    //     'secondary' => __('Secondary Menu', 'dotwfiftytwo' )
    // ) );
 
    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
    
    /**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
        );
        
        // This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'dotwfiftytwo' ),
				'secondary' => __( 'Secondary Menu', 'dotwfiftytwo' ),
				'footer' => __( 'Footer Menu', 'dotwfiftytwo' ),
				'social' => __( 'Social Links Menu', 'dotwfiftytwo' ),
			)
		);
}
endif; // dotwfiftytwo_setup
add_action( 'after_setup_theme', 'dotwfiftytwo_setup' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dotwfiftytwo_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Seearch form', 'dotwfiftytwo' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your primery menu.', 'dotwfiftytwo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => __( 'Footer left', 'dotwfiftytwo' ),
			'id'            => 'footer-left',
			'description'   => __( 'Add widgets here to appear in left coloum of the footer', 'dotwfiftytwo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => __( 'Footer middle', 'dotwfiftytwo' ),
			'id'            => 'footer-middle',
			'description'   => __( 'Add widgets here to appear in middle coloum of the footer', 'dotwfiftytwo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => __( 'Footer right', 'dotwfiftytwo' ),
			'id'            => 'footer-right',
			'description'   => __( 'Add widgets here to appear in right coloum of the footer', 'dotwfiftytwo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}

add_action( 'widgets_init', 'dotwfiftytwo_widgets_init' );





add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}