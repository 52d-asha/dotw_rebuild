<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage dotwfiftytwo
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
		<div class="site-info">
			

				<div class="footer-flex-grid">
					<div class="footer-col">
					<?php if ( is_active_sidebar( 'footer-left' ) ) : ?>
						<aside class="widget-area" role="complementary">
							<?php
							if ( is_active_sidebar( 'footer-left' ) ) {
								?>
										<div class="widget-column footer-widget-1">
										<?php dynamic_sidebar( 'footer-left' ); ?>
										</div>
									<?php
							}
							?>
						</aside><!-- .widget-area -->
					<?php endif; ?>
					</div>

					<div class="footer-col">
					<?php if ( is_active_sidebar( 'footer-middle' ) ) : ?>
						<aside class="widget-area" role="complementary">
							<?php
							if ( is_active_sidebar( 'footer-middle' ) ) {
								?>
										<div class="widget-column footer-widget-2">
										<?php dynamic_sidebar( 'footer-middle' ); ?>
										</div>
									<?php
							}
							?>
						</aside><!-- .widget-area -->
						<?php endif; ?>
					</div>
					
					<div class="footer-col">
					<?php if ( is_active_sidebar( 'footer-right' ) ) : ?>
						<aside class="widget-area" role="complementary">
							<?php
							if ( is_active_sidebar( 'footer-right' ) ) {
								?>
										<div class="widget-column footer-widget-3">
										<?php dynamic_sidebar( 'footer-right' ); ?>
										</div>
									<?php
							}
							?>
						</aside><!-- .widget-area -->
						<?php endif; ?>
					</div>
				</div>

			</div>	
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
<span class="fa-stack fa-lg">
  <i class="fa fa-circle fa-stack-2x"></i>
  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
</span>

