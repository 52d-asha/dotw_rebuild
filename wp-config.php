<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'x3$,N3Iql_Ks[U~k.2!yH9$Dvw#xU^M0+hp|!5;?dO]DMBE5:&H5H9~7E#Ar#fhf' );
define( 'SECURE_AUTH_KEY',  '_5myXL%bV/4SKG/S5w?Hq;W(eR]3]l)ZM]/|2$&Z}w7t#K%7M=C$;pT>%c!7fK3,' );
define( 'LOGGED_IN_KEY',    '1%#fxGr2`:f fB:|(*F0xFfp3wGx/IbSID?NY{AoV$A`6Z_=^o+ypw_UD;$mUS9Q' );
define( 'NONCE_KEY',        '2#Jdgn,%?( =y+3UM0>Dj;znvdS!B%h>-=<7eXGb:[(g7rjt{:B.[IL?R9ua-qrJ' );
define( 'AUTH_SALT',        'uCRFP7e)QP</%z)b6j6)I7JS@le{t=|BL^Xu3<1$EH+D/leCCY!t]jaH34Qy@IR,' );
define( 'SECURE_AUTH_SALT', 'yJ4e=5>=WuUY)< R|&CNC!&~g<`?]j%vQ,oTmp|9:O5Z055;{D)<;NRKb^dt[xDH' );
define( 'LOGGED_IN_SALT',   '(Sd?v^`EEn@<qQ;t&cl3J=M,=h)M#7{O %|#1r%!bd:R`+1GOL4IT:5N%qv%+pX^' );
define( 'NONCE_SALT',       'oNK}PrC2Kj.qV{0J%xZQ+r?C1I4?2:P3O<%,%BGLEqlSRgT]KG5I(WN}#cFQk9L0' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
